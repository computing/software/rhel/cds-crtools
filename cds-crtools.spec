Name:           cds-crtools
Summary:        A collection of tools for gathering data from, and inducing excitations on LIGO data channels. diag, diagd, diaggui, awggui, tpcmd, chndump are included along with foton, the filter design program.
Version:        3.1.2
Source:         https://git.ligo.org/cds/software/dtt/-/archive/%{version}/dtt-%{version}.tar.gz
Release:        1.1%{?dist}
License:        GPL2+
Requires: root-graf-x11 scipy
Requires: ldas-tools-framecpp >= 2.7.2
Requires: gds-gui-crtools >= 2.19.6
BuildRequires: cmake3 >= 3.13
BuildRequires: root >= 6.24.04-1.1 root < 6.25
BuildRequires: readline-devel fftw-devel zlib-devel expat-devel
BuildRequires: cyrus-sasl-devel libcurl-devel libmetaio-devel
BuildRequires: pcre-devel freetype-devel libpng-devel openssl-devel xz-devel
BuildRequires: gds-base-crtools >= 2.19 
BuildRequires: gds-frameio-devel >= 2.19 
BuildRequires: gds-gui >= 2.19.6 gds-gui-devel >= 2.19.6 gds-gui-headers >= 2.19.6
BuildRequires: ldas-tools-framecpp-devel >= 2.7.2-2
BuildRequires: nds2-client-devel >= 0.16.7
Obsoletes: gds-cdsbase < 3 gds-cdsbase-devel < 3 gds-root-crtools < 3 gds-dtt-crtools < 3 gds-dtt < 3 gds-dtt-monitors < 3
Provides: gds-dtt-crtools < 3 gds-dtt-monitors < 3 gds-dtt < 3

%description
%{summary}

%package devel
Summary:    Development files
Obsoletes:  gds-dtt-devel < 3, gds-dtt-headers < 3
Provides:   gds-dtt-devel < 3, gds-dtt-headers < 3
%description devel
Header files and libraries for building against %{name}

%package -n python2-%{name}
Summary:    Python2 files for %{name}
BuildRequires:  python2-devel
Requires:   python2-numpy
Requires:   python2-scipy
Requires:   python2-root
Requires:   gds-root-extensions-crtools, gds-root-extensions-base
Requires:   gds-base-crtools
Requires:   %{name}-devel
Requires:   %{name} = %{version}-%{release}
Obsoletes:  py-gds-dtt < 3
Provides:   py-gds-dtt < 3
%description -n python2-%{name}
%{summary}

%package -n python%{python3_pkgversion}-%{name}
Summary:    Python3 files for %{name}
BuildRequires:  python%{python3_pkgversion}-devel
Requires:   python%{python3_pkgversion}-scipy
Requires:   python%{python3_pkgversion}-numpy
Requires:   python%{python3_pkgversion}-root
Requires:   %{name} = %{version}-%{release}
Requires:   gds-root-extensions-crtools, gds-root-extensions-base
Requires:   gds-base-crtools
Requires:   %{name}-devel
Obsoletes:  py36-gds-dtt < 3
Provides:   py36-gds-dtt < 3
%{?python_provide:%python_provide python%{python3_pkgversion}-%{upname}}
%description -n python%{python3_pkgversion}-%{name}
%{summary}


%prep
%setup -q -n dtt-%{version}

%build
%cmake3 -DCMAKE_INSTALL_PREFIX=/usr -UINCLUDE_INSTALL_DIR -DCMAKE_BUILD_TYPE=Release -DENABLE_PYTHON2=yes -DENABLE_PYTHON3=yes -DGDS_INCLUDE_DIR:PATH=%{_includedir}/gds .
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install



%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_libdir}/*.so.*
%{_libdir}/root/*.so
%{_libdir}/*.pcm

%files devel
%{_libdir}/*.so
%{_includedir}/gds/*
%{_includedir}/foton/*
%{_includedir}/dmtview/*
%{_includedir}/dtt/*

%files -n python%{python3_pkgversion}-%{name}
%{python3_sitelib}/*

%files -n python2-%{name}
%{python_sitelib}/*

%changelog
* Fri Apr 15 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.2-1
* Thu Apr 14 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.1-1
* Tue Apr 12 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.1~dev2-1
* Wed Apr 6 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.1~dev1-1
* Thu Feb 10 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.0-1
* Wed Feb 9 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.0~rc4-1
* Fri Feb 4 2022 Erik von Reis <evonreis@caltech.edu> - 3.1.0~rc3-1
* Fri Feb 4 2022 Erik von Reis <evonries@caltech.edu> - 3.1.0~rc2-1
- Tightened root version to fix subversion
- Added NDS2 client build dependency
- Changed source back to tar.gz

* Thu Nov 11 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.3-2
- Tightened root version dependency to ensure patch is installed.

* Thu Nov 11 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.3-1
* Mon Oct 4 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-8
- Undef INCLUDE_INSTAll_DIR to force correct foton.py creation on koji

* Mon Oct 4 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-7
- Add prefix so that foton.py is looking in the right place for FilterFile header files.

* Thu Sep 9 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-6
- Bumped expected ldas-tools-framecpp-devel to 2.7.2-2

* Thu Sep 9 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-5
- Fixed package replacement for python 3 package.
- Added provides for gds-dtt

* Thu Aug 26 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-4
- Added build requires for ldas-tools-framecpp-devel.  Changed package requires to ldas-tools-framecpp from -devel.

* Thu Aug 26 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-3
- Added Requires for ldas-tools-framecpp-devel and gds-gui-crtools

* Wed Aug 25 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-2
- Added gds-dtt-monitors obsolete

* Wed Aug 25 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.2-1

* Fri Aug 13 2021 Erik von REis <evonreis@caltech.edu> - 3.0.0-2
- Loosened constraints on Obsoletes of GDS packages to any version less than 3
- Added osbsolescence for gds-dtt package

* Fri Aug 13 2021 Erik von Reis <evonreis@caltech.edu> - 3.0.0-1
- added some version constraints

* Wed Jul 21 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc10-2
- corrected foton headerfile location

* Wed Jul 14 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc10-1

* Tue Jun 29 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc9-1
- added missing header files to devel
- dropped conflicts with gds-gui, added dependencies on gds-gui

* Wed May 19 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc5-1
- removed libpanel.  libRpanel remains

* Wed May 19 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc4-1
- libgdsplot added back in.
- libpanel added

* Tue May 18 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc3-1
- libfilterfile.so added

* Tue May 18 2021 Erik von Reis <evonreis@caltech.edu> - 0.5.0~rc2-1
- ezcatools dropped in favor of separate package

* Wed Apr 28 2021 Erik von Reis <evonreis@caltech.edu> - 0.4.0-2
- Packages now obsolete conflicting GDS packages.

* Tue Mar 9 2021 Erik von Reis <evonreis@caltech.edu> - 0.4.0
- Built against GDS 2.19.4 and ROOT 6.22/06
- Version 0.4.0 intial release

* Tue Jan 19 2021 Erik von Reis <evonreis@caltech.edu> - 0.3.0-2
- Fixed cmake to find EPICS libraries

* Mon Jan 18 2021 Erik von Reis <evonreis@caltech.edu> - 0.3.0-2
- Added ezca tools: ezcaread, ezcawrite, ezcaswitch, ezcastep, ezcademod, ezcaservo

* Wed Oct 7 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0

* Fri Oct 2 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0~rc13-2
- Obsoletes gds-cdsbase and gds-cdsbase-devel to try to ease upgrade

* Fri Oct 2 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0~rc13-1

* Wed Sep 2 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0~rc11-1
- Changed to build off of regular GDS distro instead of gds-cdsbase
- dmtviewer and lidax added

* Tue Aug 18 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0~rc8-1
- Changed source extension to .xz
- Added explicit build directory to %%setup macro

* Sat Jul 25 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0~rc3-1
* Fri Jun 25 2020 Erik von Reis <evonreis@caltech.edu> - 0.3.0~rc2-1
* Thu Jun 18 2020 Erik von Reis <evonreis@caltech.edu> - 0.2.0~rc17-1
- Update to rc17
- Add missing Requires:

* Sat Jun 13 2020 Michael Thomas <michael.thomas@LIGO.ORG> - 0.2.0~rc16-2
- Split into subpackages

* Fri Jun 12 2020 Erik von Reis <evonreis@caltech.edu> - 0.2.0~rc16-1
- Some Requires added to run foton.py.
- .pcm files for root dictionaries installed.

* Fri Jun 12 2020 Erik von Reis <evonreis@caltech.edu> - 0.2.0~rc15-1
- Update to rc15

* Fri Jun 12 2020 Michael Thomas <michael.thomas@LIGO.ORG> - 0.2.0~rc14-3
- packaging fixes

* Fri Jun 12 2020 Erik von Reis <evonreis@caltech.edu> - 0.2.0~rc14-2

* Sat Feb 1 2020 Michael Thomas <michael.thomas@LIGO.ORG> - 0.1.0_rc18-1
  Initial package
